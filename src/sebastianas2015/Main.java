package sebastianas2015;

/**
 *
 * @author helder
 */
public class Main implements Comparable<Main>{
    
    String streamLink="";
    boolean streamOnline = false;

    public void setStreamLink(String streamLink) {
        this.streamLink = streamLink;
    }

    public void setStreamOnline(boolean streamOnline) {
        this.streamOnline = streamOnline;
    }
    
    
    public Main(){
        
    }

    public Main(boolean streamOnline, String streamLink) {
        this.streamLink = streamLink;
        this.streamOnline = streamOnline;
    }

    @Override
    public String toString() {
        return streamLink;
    }
    
    @Override
    public boolean equals(Object obj){
        Main event = (Main) obj;
        boolean result = true;
        if (!this.streamLink.equals(event.streamLink))
            result = false;
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public int compareTo(Main o) {
        return this.streamLink.compareTo(o.streamLink);
    }

}
