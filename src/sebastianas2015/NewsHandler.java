package sebastianas2015;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import org.apache.commons.net.ftp.FTPClient;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author helder
 */
public class NewsHandler {

    private static final String FILENAME = "news";

    private final String paddress;
    private final String ftp_address;
    private final String ftp_folder;
    private final String username;
    private final String password;

    private ArrayList<Long> deleted;

    public NewsHandler(String paddress, String ftp_address, String ftp_folder, String username, String password) {
        this.paddress = paddress;
        this.ftp_address = ftp_address;
        this.username = username;
        this.password = password;
        this.ftp_folder = ftp_folder;
        deleted = new ArrayList();
    }

    public Document parse(URL url) {
        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(url);
            return document;
        } catch (DocumentException ex) {
            return null;
        }
    }

    public void setDeletedItems(ArrayList<Long> list) {
        deleted = list;
    }

    public ArrayList<Long> getDeletedItems() {
        return deleted;
    }

    public NewsEvent[] loadFromFile() {
        URL url = null;
        try {
            url = new URL(paddress + FILENAME + ".xml");
        } catch (MalformedURLException ex) {
            Logger.getLogger(NewsHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        Document doc = parse(url);

        if (doc == null) {
            return null;
        }

        Queue<NewsEvent> news = new PriorityQueue<>();
        Element root = doc.getRootElement();

        for (Iterator i = root.elementIterator(); i.hasNext();) {
            Element item = (Element) i.next();
            if (item.getName().equals("event")) {
                NewsEvent event = new NewsEvent();
                for (Iterator j = item.elementIterator(); j.hasNext();) {
                    Element el = (Element) j.next();
                    switch (el.getName()) {
                        case "uuid":
                            event.setUUID(Long.parseLong((String) el.getData()));
                            break;
                        case "title":
                            event.setTitle((String) el.getData());
                            break;
                        case "description":
                            event.setDesc((String) el.getData());
                            break;
                        case "image":
                            event.setImage((String) el.getData());
                            break;
                        case "video":
                            event.setVideo((String) el.getData());
                            break;
                        case "day":
                            event.setDay(Integer.parseInt((String) el.getData()));
                            break;
                        case "month":
                            event.setMonth(Integer.parseInt((String) el.getData()));
                            break;
                        case "hour":
                            event.setHour(Integer.parseInt((String) el.getData()));
                            break;
                        case "minutes":
                            event.setMinutes(Integer.parseInt((String) el.getData()));
                            break;
                        case "notify":
                            if (((String) el.getData()).equals("true")) {
                                event.setNotify(true);
                            } else {
                                event.setNotify(false);
                            }
                            break;
                    }
                }
                news.add(event);
            }
            else{
                for (Iterator j = item.elementIterator(); j.hasNext();) {
                    Element el = (Element) j.next();
                    deleted.add(Long.parseLong((String) el.getData()));
                }
            }

        }

        NewsEvent[] array = new NewsEvent[news.size()];
        return news.toArray(array);
    }

    public boolean saveToFile(Object[] objs, JTextArea textarea) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(FILENAME);

        NewsEvent[] news = Arrays.asList(objs).toArray(new NewsEvent[objs.length]);

        textarea.append("Criando documento... ");
        textarea.update(textarea.getGraphics());
        for (NewsEvent event : news) {
            Element xml_event = root.addElement("event");
            xml_event.addElement("uuid")
                    .addText("" + event.uuid);
            xml_event.addElement("title")
                    .addText(event.title);
            if (!event.desc.equals("")) {
                xml_event.addElement("description")
                        .addText(event.desc);
            }
            if (!event.image.equals("")) {
                xml_event.addElement("image")
                        .addText(event.image);
            }
            if (!event.video.equals("")) {
                xml_event.addElement("video")
                        .addText(event.video);
            }
            xml_event.addElement("day")
                    .addText("" + event.day);
            xml_event.addElement("month")
                    .addText("" + event.month);
            xml_event.addElement("hour")
                    .addText("" + event.hour);
            xml_event.addElement("minutes")
                    .addText("" + event.minutes);
            if (event.notify) {
                xml_event.addElement("notify")
                        .addText("true");
            } else {
                xml_event.addElement("notify")
                        .addText("false");
            }
        }
        Element xml_deletedEvents = root.addElement("deleted_events");
        deleted.stream().forEach((uuid) -> {
            xml_deletedEvents.addElement("deleted_event")
                    .addText("" + uuid);
        });
        textarea.append("OK\n");
        textarea.update(textarea.getGraphics());
        return saveDocument(document, textarea);
    }

    public boolean saveDocument(Document document, JTextArea textarea) {

        try {
            textarea.append("Guardando em ficheiro temporario... ");
            textarea.update(textarea.getGraphics());
            File file = File.createTempFile(FILENAME, ".xml");
            //OutputFormat format = OutputFormat.createPrettyPrint();
            XMLWriter writer = new XMLWriter(new FileWriter(file));
            writer.write(document);
            writer.close();
            textarea.append("OK\n");
            textarea.update(textarea.getGraphics());
            return sendFile(file, textarea);
        } catch (IOException ex) {
            textarea.append("ERRO!!\n");
            textarea.update(textarea.getGraphics());
            Logger.getLogger(NewsHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean sendFile(File file, JTextArea textarea) {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        boolean result = false;
        try {
            textarea.append("Criando ligação por FTP ao servidor... ");
            textarea.update(textarea.getGraphics());
            client.connect(ftp_address);
            textarea.append("OK\nEfectuando login... ");
            textarea.update(textarea.getGraphics());
            client.login(username, password);
            textarea.append("OK\nGuardando ficheiro no servidor... ");
            textarea.update(textarea.getGraphics());
            fis = new FileInputStream(file);
            client.storeFile(ftp_folder + FILENAME + ".xml", fis);
            textarea.append("OK\nEfectuando logout... ");
            textarea.update(textarea.getGraphics());
            client.logout();
            textarea.append("OK\nApagando Ficheiro temporario... ");
            textarea.update(textarea.getGraphics());
            file.delete();
            textarea.append("OK\n");
            textarea.update(textarea.getGraphics());
            result = true;
        } catch (IOException ex) {
            textarea.append("ERRO!!\n");
            textarea.update(textarea.getGraphics());
            Logger.getLogger(NewsHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                textarea.append("Desligando do servidor... ");
                textarea.update(textarea.getGraphics());
                client.disconnect();
                textarea.append("OK\n");
            } catch (IOException ex) {
                textarea.append("ERRO!!\n");
                Logger.getLogger(NewsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

}
