package sebastianas2015;

import java.util.UUID;

/**
 *
 * @author helder
 */
public class Sponsor implements Comparable<Sponsor>{
    
    long uuid;
    String title="", image="", link="";

    public void setUUID(long uuid){
        this.uuid = uuid;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
    public Sponsor(){
        this.uuid = UUID.randomUUID().getMostSignificantBits();
    }

    public Sponsor(String title, String image, String link) {

        this.uuid = UUID.randomUUID().getMostSignificantBits();
        this.title = title;
        this.image = image;
        this.link = link;
    }

    @Override
    public String toString() {
        return title;
    }
    
    @Override
    public boolean equals(Object obj){
        Sponsor event = (Sponsor) obj;
        if (this.title.equals(event.title))
            return true;
        return this.uuid == event.uuid;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public int compareTo(Sponsor o) {
        return 0;
    }

}
