package sebastianas2015;

import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author helder
 */
public class SebsJFrame extends javax.swing.JFrame {

    private final ProgramHandler program_handler;
    private final NewsHandler news_handler;
    private final SponsorsHandler sponsors_handler;
    private final MainHandler main_handler;
    private Main mainSettings;

    private final DefaultListModel<ProgramEvent> program_events;
    private final ArrayList<Long> deleted_program;
    private final DefaultListModel<NewsEvent> news_events;
    private final ArrayList<Long> deleted_news;
    private final DefaultListModel<Sponsor> sponsors;
    private final ArrayList<Long> deleted_sponsors;
    private final PrefsFrame prefs;
    private boolean edit;

    private final String paddress;
    private final String ftp_address;
    private final String ftp_folder;
    private final String username;
    private final String password;

    public SebsJFrame() {
        edit = false;
        prefs = new PrefsFrame();
        prefs.setLocationRelativeTo(null);
        initComponents();

        paddress = prefs.getPublicAddress();
        ftp_address = prefs.getFtpAddress();
        ftp_folder = prefs.getFtpFolder();
        username = prefs.getUsername();
        password = prefs.getPassword();

        program_handler = new ProgramHandler(paddress, ftp_address, ftp_folder, username, password);
        news_handler = new NewsHandler(paddress, ftp_address, ftp_folder, username, password);
        sponsors_handler = new SponsorsHandler(paddress, ftp_address, ftp_folder, username, password);
        main_handler = new MainHandler(paddress, ftp_address, ftp_folder, username, password);

        program_events = new DefaultListModel<>();
        news_events = new DefaultListModel<>();
        sponsors = new DefaultListModel<>();
        ProgramList.setModel(program_events);
        NewsList.setModel(news_events);
        SponsorsList.setModel(sponsors);

        ProgramEvent[] existingEvents = program_handler.loadFromFile();
        deleted_program = program_handler.getDeletedItems();
        if (existingEvents != null) {
            for (ProgramEvent existingEvent : existingEvents) {
                program_events.addElement(existingEvent);
            }
        }

        NewsEvent[] existingNews = news_handler.loadFromFile();
        deleted_news = news_handler.getDeletedItems();
        if (existingNews != null) {
            for (NewsEvent existingNew : existingNews) {
                news_events.addElement(existingNew);
            }
        }

        Sponsor[] existingSponsors = sponsors_handler.loadFromFile();
        deleted_sponsors = sponsors_handler.getDeletedItems();
        if (existingSponsors != null) {
            for (Sponsor existingSponsor : existingSponsors) {
                sponsors.addElement(existingSponsor);
            }
        }

        mainSettings = main_handler.loadFromFile();

        if (mainSettings != null) {
            main_livestream_checkbox.setSelected(mainSettings.streamOnline);
            main_livestream_link.setEnabled(mainSettings.streamOnline);
            main_livestream_link.setText(mainSettings.streamLink);
        } else {
            mainSettings = new Main();
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        panel_main = new javax.swing.JPanel();
        main_livestream_panel = new javax.swing.JPanel();
        main_livestream_link = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        main_livestream_checkbox = new javax.swing.JCheckBox();
        jLabel18 = new javax.swing.JLabel();
        panel_program = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ProgramList = new javax.swing.JList();
        program_button_edit = new javax.swing.JButton();
        program_button_delete = new javax.swing.JButton();
        program_button_new = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        program_title_textfield = new javax.swing.JTextField();
        program_image_textfield = new javax.swing.JTextField();
        program_video_textfield = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        program_month_combo = new javax.swing.JComboBox();
        program_day_combo = new javax.swing.JComboBox();
        program_hour_spin = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();
        program_minutes_spin = new javax.swing.JSpinner();
        program_cancel_button = new javax.swing.JButton();
        program_ok_button = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        program_desc_textfield = new javax.swing.JTextArea();
        panel_news = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        NewsList = new javax.swing.JList();
        news_button_edit = new javax.swing.JButton();
        news_button_delete = new javax.swing.JButton();
        news_button_new = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        news_title_textfield = new javax.swing.JTextField();
        news_image_textfield = new javax.swing.JTextField();
        news_video_textfield = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        news_month_combo = new javax.swing.JComboBox();
        news_day_combo = new javax.swing.JComboBox();
        news_hour_spin = new javax.swing.JSpinner();
        jLabel16 = new javax.swing.JLabel();
        news_minutes_spin = new javax.swing.JSpinner();
        news_notify_checkbox = new javax.swing.JCheckBox();
        news_ok_button = new javax.swing.JButton();
        news_cancel_button = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        news_desc_textfield = new javax.swing.JTextArea();
        panel_sponsors = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        SponsorsList = new javax.swing.JList();
        sponsors_button_edit = new javax.swing.JButton();
        sponsors_button_delete = new javax.swing.JButton();
        sponsors_button_new = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        sponsors_title_textfield = new javax.swing.JTextField();
        sponsors_image_textfield = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        sponsors_cancel_button = new javax.swing.JButton();
        sponsors_ok_button = new javax.swing.JButton();
        sponsors_link_textfield = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        sponsors_button_down = new javax.swing.JButton();
        sponsors_button_up = new javax.swing.JButton();
        button_saveall = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sebastianas 2015");
        setResizable(false);

        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        jTabbedPane1.setAutoscrolls(true);

        panel_main.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        main_livestream_panel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        main_livestream_link.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        main_livestream_link.setEnabled(false);

        jLabel17.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel17.setText("URL:");

        main_livestream_checkbox.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        main_livestream_checkbox.setText("Online");
        main_livestream_checkbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                main_livestream_checkboxActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel18.setText("Live Stream:");

        javax.swing.GroupLayout main_livestream_panelLayout = new javax.swing.GroupLayout(main_livestream_panel);
        main_livestream_panel.setLayout(main_livestream_panelLayout);
        main_livestream_panelLayout.setHorizontalGroup(
            main_livestream_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(main_livestream_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(main_livestream_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18)
                    .addComponent(main_livestream_checkbox)
                    .addGroup(main_livestream_panelLayout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(main_livestream_link, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        main_livestream_panelLayout.setVerticalGroup(
            main_livestream_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(main_livestream_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addGap(10, 10, 10)
                .addComponent(main_livestream_checkbox)
                .addGap(18, 18, 18)
                .addGroup(main_livestream_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(main_livestream_link, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panel_mainLayout = new javax.swing.GroupLayout(panel_main);
        panel_main.setLayout(panel_mainLayout);
        panel_mainLayout.setHorizontalGroup(
            panel_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_mainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(main_livestream_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(253, Short.MAX_VALUE))
        );
        panel_mainLayout.setVerticalGroup(
            panel_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_mainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(main_livestream_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(319, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Geral", panel_main);

        panel_program.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        ProgramList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                ProgramListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(ProgramList);

        program_button_edit.setText("Editar");
        program_button_edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                program_button_editActionPerformed(evt);
            }
        });

        program_button_delete.setText("Eliminar");
        program_button_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                program_button_deleteActionPerformed(evt);
            }
        });

        program_button_new.setText("Novo");
        program_button_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                program_button_newActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel1.setText("Título:");

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel2.setText("Descrição:");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel3.setText("Imagem (url):");

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel4.setText("Video (url):");

        program_title_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        program_title_textfield.setEnabled(false);

        program_image_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        program_image_textfield.setEnabled(false);

        program_video_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        program_video_textfield.setEnabled(false);

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel5.setText("Dia:");

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel6.setText("Mês:");

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel7.setText("Hora:");

        program_month_combo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        program_month_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
        program_month_combo.setEnabled(false);

        program_day_combo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        program_day_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        program_day_combo.setEnabled(false);

        program_hour_spin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        program_hour_spin.setEnabled(false);
        program_hour_spin.setValue(22);

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel8.setText(":");

        program_minutes_spin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        program_minutes_spin.setEnabled(false);
        program_minutes_spin.setValue(30);

        program_cancel_button.setText("Cancelar");
        program_cancel_button.setEnabled(false);
        program_cancel_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                program_cancel_buttonActionPerformed(evt);
            }
        });

        program_ok_button.setText("Confirmar");
        program_ok_button.setEnabled(false);
        program_ok_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                program_ok_buttonActionPerformed(evt);
            }
        });

        program_desc_textfield.setColumns(20);
        program_desc_textfield.setRows(5);
        program_desc_textfield.setEnabled(false);
        jScrollPane5.setViewportView(program_desc_textfield);

        javax.swing.GroupLayout panel_programLayout = new javax.swing.GroupLayout(panel_program);
        panel_program.setLayout(panel_programLayout);
        panel_programLayout.setHorizontalGroup(
            panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_programLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_programLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panel_programLayout.createSequentialGroup()
                                    .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panel_programLayout.createSequentialGroup()
                                            .addComponent(jLabel1)
                                            .addGap(36, 36, 36))
                                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING))
                                    .addGap(4, 4, 4))
                                .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(program_video_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(program_title_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(program_image_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_programLayout.createSequentialGroup()
                                .addComponent(program_day_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6)
                                .addGap(4, 4, 4)
                                .addComponent(program_month_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panel_programLayout.createSequentialGroup()
                                        .addComponent(program_ok_button)
                                        .addGap(13, 13, 13)
                                        .addComponent(program_cancel_button))
                                    .addGroup(panel_programLayout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(program_hour_spin, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(program_minutes_spin, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel_programLayout.createSequentialGroup()
                        .addComponent(program_button_new)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(program_button_edit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(program_button_delete)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panel_programLayout.setVerticalGroup(
            panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_programLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_programLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(program_button_new)
                            .addComponent(program_button_edit)
                            .addComponent(program_button_delete)))
                    .addGroup(panel_programLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(program_title_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(program_image_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(program_video_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(program_month_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(program_day_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(program_hour_spin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(program_minutes_spin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(panel_programLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(program_ok_button)
                    .addComponent(program_cancel_button))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Programa", panel_program);

        panel_news.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        NewsList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                NewsListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(NewsList);

        news_button_edit.setText("Editar");
        news_button_edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                news_button_editActionPerformed(evt);
            }
        });

        news_button_delete.setText("Eliminar");
        news_button_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                news_button_deleteActionPerformed(evt);
            }
        });

        news_button_new.setText("Novo");
        news_button_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                news_button_newActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel9.setText("Título:");

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel10.setText("Descrição:");

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel11.setText("Imagem (url):");

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel12.setText("Video (url):");

        news_title_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        news_title_textfield.setEnabled(false);

        news_image_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        news_image_textfield.setEnabled(false);

        news_video_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        news_video_textfield.setEnabled(false);

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel13.setText("Dia:");

        jLabel14.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel14.setText("Mês:");

        jLabel15.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel15.setText("Hora:");

        news_month_combo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        news_month_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
        news_month_combo.setEnabled(false);

        news_day_combo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        news_day_combo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        news_day_combo.setEnabled(false);

        news_hour_spin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        news_hour_spin.setEnabled(false);
        news_hour_spin.setValue(22);

        jLabel16.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel16.setText(":");

        news_minutes_spin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        news_minutes_spin.setEnabled(false);
        news_minutes_spin.setValue(30);

        news_notify_checkbox.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        news_notify_checkbox.setText("Com notificação");
        news_notify_checkbox.setEnabled(false);
        news_notify_checkbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                news_notify_checkboxActionPerformed(evt);
            }
        });

        news_ok_button.setText("Confirmar");
        news_ok_button.setEnabled(false);
        news_ok_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                news_ok_buttonActionPerformed(evt);
            }
        });

        news_cancel_button.setText("Cancelar");
        news_cancel_button.setEnabled(false);
        news_cancel_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                news_cancel_buttonActionPerformed(evt);
            }
        });

        news_desc_textfield.setColumns(20);
        news_desc_textfield.setRows(5);
        news_desc_textfield.setEnabled(false);
        jScrollPane4.setViewportView(news_desc_textfield);

        javax.swing.GroupLayout panel_newsLayout = new javax.swing.GroupLayout(panel_news);
        panel_news.setLayout(panel_newsLayout);
        panel_newsLayout.setHorizontalGroup(
            panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_newsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panel_newsLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_newsLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(panel_newsLayout.createSequentialGroup()
                                            .addComponent(jLabel9)
                                            .addGap(36, 36, 36))
                                        .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING))
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel11))
                                .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(news_video_textfield, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                                    .addComponent(news_title_textfield, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                                    .addComponent(news_image_textfield, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                                    .addComponent(jScrollPane4)))
                            .addGroup(panel_newsLayout.createSequentialGroup()
                                .addGap(89, 89, 89)
                                .addComponent(news_notify_checkbox))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_newsLayout.createSequentialGroup()
                        .addComponent(news_button_new)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(news_button_edit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(news_button_delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
                        .addComponent(jLabel13)
                        .addGap(0, 0, 0)
                        .addComponent(news_day_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14)
                        .addGap(4, 4, 4)
                        .addComponent(news_month_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_newsLayout.createSequentialGroup()
                                .addComponent(news_ok_button)
                                .addGap(13, 13, 13)
                                .addComponent(news_cancel_button))
                            .addGroup(panel_newsLayout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(news_hour_spin, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(news_minutes_spin, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panel_newsLayout.setVerticalGroup(
            panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_newsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_newsLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(news_button_new)
                            .addComponent(news_button_edit)
                            .addComponent(news_button_delete)))
                    .addGroup(panel_newsLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(news_title_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(news_image_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addGap(18, 18, 18)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(news_video_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addComponent(news_notify_checkbox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)
                            .addComponent(jLabel15)
                            .addComponent(news_month_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(news_day_combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(news_hour_spin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel16)
                            .addComponent(news_minutes_spin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(panel_newsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(news_ok_button)
                    .addComponent(news_cancel_button))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Novidades", panel_news);

        panel_sponsors.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        SponsorsList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                SponsorsListValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(SponsorsList);

        sponsors_button_edit.setText("Editar");
        sponsors_button_edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsors_button_editActionPerformed(evt);
            }
        });

        sponsors_button_delete.setText("Eliminar");
        sponsors_button_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsors_button_deleteActionPerformed(evt);
            }
        });

        sponsors_button_new.setText("Novo");
        sponsors_button_new.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsors_button_newActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel19.setText("Nome:");

        jLabel21.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel21.setText("Imagem (url):");

        sponsors_title_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        sponsors_title_textfield.setEnabled(false);

        sponsors_image_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        sponsors_image_textfield.setEnabled(false);

        jLabel26.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel26.setText(":");

        sponsors_cancel_button.setText("Cancelar");
        sponsors_cancel_button.setEnabled(false);
        sponsors_cancel_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsors_cancel_buttonActionPerformed(evt);
            }
        });

        sponsors_ok_button.setText("Confirmar");
        sponsors_ok_button.setEnabled(false);
        sponsors_ok_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsors_ok_buttonActionPerformed(evt);
            }
        });

        sponsors_link_textfield.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        sponsors_link_textfield.setEnabled(false);

        jLabel22.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        jLabel22.setText("Link:");

        sponsors_button_down.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sebastianas2015/resources/down-arrow.png"))); // NOI18N
        sponsors_button_down.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsors_button_downActionPerformed(evt);
            }
        });

        sponsors_button_up.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sebastianas2015/resources/up-arrow.png"))); // NOI18N
        sponsors_button_up.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsors_button_upActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_sponsorsLayout = new javax.swing.GroupLayout(panel_sponsors);
        panel_sponsors.setLayout(panel_sponsorsLayout);
        panel_sponsorsLayout.setHorizontalGroup(
            panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_sponsorsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sponsors_button_up, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sponsors_button_down, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_sponsorsLayout.createSequentialGroup()
                        .addComponent(sponsors_button_new)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sponsors_button_edit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sponsors_button_delete))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_sponsorsLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(sponsors_ok_button)
                        .addGap(13, 13, 13)
                        .addComponent(sponsors_cancel_button))
                    .addGroup(panel_sponsorsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_sponsorsLayout.createSequentialGroup()
                                .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel21))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                                .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(sponsors_image_textfield, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(sponsors_title_textfield, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panel_sponsorsLayout.createSequentialGroup()
                                .addGap(329, 329, 329)
                                .addComponent(jLabel26)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(panel_sponsorsLayout.createSequentialGroup()
                                .addComponent(jLabel22)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(sponsors_link_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        panel_sponsorsLayout.setVerticalGroup(
            panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_sponsorsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_sponsorsLayout.createSequentialGroup()
                        .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_sponsorsLayout.createSequentialGroup()
                                .addComponent(sponsors_button_up, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sponsors_button_down, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(sponsors_button_new)
                            .addComponent(sponsors_button_edit)
                            .addComponent(sponsors_button_delete)))
                    .addGroup(panel_sponsorsLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(sponsors_title_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(sponsors_image_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(sponsors_link_textfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addGroup(panel_sponsorsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(sponsors_ok_button)
                            .addComponent(sponsors_cancel_button))
                        .addGap(142, 142, 142)
                        .addComponent(jLabel26)))
                .addContainerGap(67, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Patrocinadores", panel_sponsors);

        button_saveall.setText("Guardar");
        button_saveall.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_saveallActionPerformed(evt);
            }
        });

        jMenu1.setText("Ficheiro");

        jMenuItem1.setText("Sair");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Editar");

        jMenuItem2.setText("Preferências");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Sobre");

        jMenuItem3.setText("Sobre a aplicação");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(button_saveall, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(button_saveall)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void main_livestream_checkboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_main_livestream_checkboxActionPerformed
        if (main_livestream_checkbox.isSelected()) {
            main_livestream_link.setEnabled(true);
        } else {
            main_livestream_link.setEnabled(false);
        }
    }//GEN-LAST:event_main_livestream_checkboxActionPerformed

    private void news_notify_checkboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_news_notify_checkboxActionPerformed
        if (news_notify_checkbox.isSelected()) {
            enableNotify();
        } else {
            disableNotify();
        }
    }//GEN-LAST:event_news_notify_checkboxActionPerformed

    private void program_button_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_program_button_newActionPerformed
        clearProgram();
        enableProgram();
    }//GEN-LAST:event_program_button_newActionPerformed

    private void program_button_editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_program_button_editActionPerformed
        if (ProgramList.getSelectedIndex() < 0) {
            return;
        }
        edit = true;
        enableProgram();
    }//GEN-LAST:event_program_button_editActionPerformed

    private void program_button_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_program_button_deleteActionPerformed
        if (ProgramList.getSelectedIndex() < 0) {
            return;
        }
        ProgramEvent event = (ProgramEvent) program_events.elementAt(ProgramList.getSelectedIndex());
        deleted_program.add(event.uuid);
        program_events.removeElementAt(ProgramList.getSelectedIndex());
        disableProgram();
        clearProgram();
    }//GEN-LAST:event_program_button_deleteActionPerformed

    private void news_button_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_news_button_newActionPerformed
        clearNews();
        enableNews();
        Calendar now = Calendar.getInstance();
        news_day_combo.setSelectedIndex(now.get(Calendar.DAY_OF_MONTH) - 1);
        news_month_combo.setSelectedIndex(now.get(Calendar.MONTH));
        news_hour_spin.setValue(now.get(Calendar.HOUR));
        news_minutes_spin.setValue(now.get(Calendar.MINUTE));
    }//GEN-LAST:event_news_button_newActionPerformed

    private void news_button_editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_news_button_editActionPerformed
        if (NewsList.getSelectedIndex() < 0) {
            return;
        }
        edit = true;
        enableNews();
    }//GEN-LAST:event_news_button_editActionPerformed

    private void news_button_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_news_button_deleteActionPerformed
        if (NewsList.getSelectedIndex() < 0) {
            return;
        }

        NewsEvent event = (NewsEvent) news_events.elementAt(NewsList.getSelectedIndex());
        deleted_news.add(event.uuid);

        news_events.removeElementAt(NewsList.getSelectedIndex());
        disableNews();
    }//GEN-LAST:event_news_button_deleteActionPerformed

    private void sponsors_button_newActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsors_button_newActionPerformed
        clearSponsors();
        enableSponsors();
    }//GEN-LAST:event_sponsors_button_newActionPerformed

    private void sponsors_button_editActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsors_button_editActionPerformed
        if (SponsorsList.getSelectedIndex() < 0) {
            return;
        }
        edit = true;
        enableSponsors();
    }//GEN-LAST:event_sponsors_button_editActionPerformed

    private void sponsors_button_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsors_button_deleteActionPerformed
        if (SponsorsList.getSelectedIndex() < 0) {
            return;
        }

        Sponsor sponsor = (Sponsor) sponsors.elementAt(SponsorsList.getSelectedIndex());
        deleted_sponsors.add(sponsor.uuid);

        sponsors.removeElementAt(SponsorsList.getSelectedIndex());
        disableSponsors();
    }//GEN-LAST:event_sponsors_button_deleteActionPerformed

    private void program_cancel_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_program_cancel_buttonActionPerformed
        disableProgram();
        edit = false;
    }//GEN-LAST:event_program_cancel_buttonActionPerformed

    private void program_ok_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_program_ok_buttonActionPerformed
        if (program_title_textfield.getText().equals("")) {
            return;
        }

        ProgramEvent event = new ProgramEvent(program_title_textfield.getText(),
                program_desc_textfield.getText(),
                program_image_textfield.getText(),
                program_video_textfield.getText(),
                Integer.parseInt(program_day_combo.getSelectedItem().toString()),
                Integer.parseInt(program_month_combo.getSelectedItem().toString()),
                (int) program_hour_spin.getValue(),
                (int) program_minutes_spin.getValue());

        ProgramEvent old = null;
        if (edit) {
            old = (ProgramEvent) program_events.getElementAt(ProgramList.getSelectedIndex());
            event.setUUID(old.uuid);
            if (ProgramList.getSelectedIndex() < 0) {
                return;
            }
            program_events.removeElementAt(ProgramList.getSelectedIndex());
        }

        if (program_events.contains(event)) {
            if (old != null) {
                program_events.addElement(old);
            }
            JOptionPane.showMessageDialog(null, "Evento já existe.", "Erro", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        program_events.addElement(event);
        clearProgram();
        disableProgram();
        edit = false;
    }//GEN-LAST:event_program_ok_buttonActionPerformed

    private void ProgramListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_ProgramListValueChanged
        if (ProgramList.getSelectedIndex() < 0) {
            return;
        }
        ProgramEvent event = (ProgramEvent) program_events.getElementAt(ProgramList.getSelectedIndex());

        program_title_textfield.setText(event.title);
        program_desc_textfield.setText(event.desc);
        program_image_textfield.setText(event.image);
        program_video_textfield.setText(event.video);
        program_day_combo.setSelectedIndex(event.day - 1);
        program_month_combo.setSelectedIndex(event.month - 1);
        program_hour_spin.setValue(event.hour);
        program_minutes_spin.setValue(event.minutes);
        disableProgram();
        edit = false;
    }//GEN-LAST:event_ProgramListValueChanged

    private void news_ok_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_news_ok_buttonActionPerformed
        if (news_title_textfield.getText().equals("")) {
            return;
        }

        boolean notify = false;
        if (news_notify_checkbox.isSelected()) {
            notify = true;
        }

        NewsEvent event = new NewsEvent(news_title_textfield.getText(),
                news_desc_textfield.getText(),
                news_image_textfield.getText(),
                news_video_textfield.getText(),
                Integer.parseInt(news_day_combo.getSelectedItem().toString()),
                Integer.parseInt(news_month_combo.getSelectedItem().toString()),
                (int) news_hour_spin.getValue(),
                (int) news_minutes_spin.getValue(),
                notify);

        NewsEvent old = null;
        if (edit) {
            old = (NewsEvent) news_events.getElementAt(NewsList.getSelectedIndex());
            event.setUUID(old.uuid);
            if (NewsList.getSelectedIndex() < 0) {
                return;
            }
            news_events.removeElementAt(NewsList.getSelectedIndex());
        }

        if (news_events.contains(event)) {
            if (old != null) {
                news_events.addElement(old);
            }
            JOptionPane.showMessageDialog(null, "Novidade já existe.", "Erro", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        news_events.addElement(event);
        clearNews();
        disableNews();
        edit = false;
    }//GEN-LAST:event_news_ok_buttonActionPerformed

    private void news_cancel_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_news_cancel_buttonActionPerformed
        disableNews();
        edit = false;
    }//GEN-LAST:event_news_cancel_buttonActionPerformed

    private void NewsListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_NewsListValueChanged
        if (NewsList.getSelectedIndex() < 0) {
            return;
        }
        NewsEvent event = (NewsEvent) news_events.getElementAt(NewsList.getSelectedIndex());

        news_title_textfield.setText(event.title);
        news_desc_textfield.setText(event.desc);
        news_image_textfield.setText(event.image);
        news_video_textfield.setText(event.video);
        news_day_combo.setSelectedIndex(event.day - 1);
        news_month_combo.setSelectedIndex(event.month - 1);
        news_hour_spin.setValue(event.hour);
        news_minutes_spin.setValue(event.minutes);
        news_notify_checkbox.setSelected(event.notify);

        disableNews();
        edit = false;
    }//GEN-LAST:event_NewsListValueChanged

    private void sponsors_cancel_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsors_cancel_buttonActionPerformed
        disableSponsors();
        edit = false;
    }//GEN-LAST:event_sponsors_cancel_buttonActionPerformed

    private void sponsors_ok_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsors_ok_buttonActionPerformed
        if (sponsors_title_textfield.getText().equals("")) {
            return;
        }

        Sponsor event = new Sponsor(sponsors_title_textfield.getText(),
                sponsors_image_textfield.getText(),
                sponsors_link_textfield.getText());

        Sponsor old = null;
        if (edit) {
            old = (Sponsor) sponsors.getElementAt(SponsorsList.getSelectedIndex());
            event.setUUID(old.uuid);
            if (SponsorsList.getSelectedIndex() < 0) {
                return;
            }
            sponsors.removeElementAt(SponsorsList.getSelectedIndex());
        }

        if (sponsors.contains(event)) {
            if (old != null) {
                sponsors.addElement(old);
            }
            JOptionPane.showMessageDialog(null, "Patrocinador já existe.", "Erro", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        sponsors.addElement(event);
        clearSponsors();
        disableSponsors();
        edit = false;
    }//GEN-LAST:event_sponsors_ok_buttonActionPerformed

    private void SponsorsListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_SponsorsListValueChanged
        if (SponsorsList.getSelectedIndex() < 0) {
            return;
        }
        Sponsor event = (Sponsor) sponsors.getElementAt(SponsorsList.getSelectedIndex());

        sponsors_title_textfield.setText(event.title);
        sponsors_image_textfield.setText(event.image);
        sponsors_link_textfield.setText(event.link);

        disableSponsors();
        edit = false;
    }//GEN-LAST:event_SponsorsListValueChanged

    private void button_saveallActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_saveallActionPerformed
        if (main_livestream_checkbox.isSelected() && main_livestream_link.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Link do Live Stream vazio.", "Erro", JOptionPane.INFORMATION_MESSAGE);
        } else {
            Working working = new Working();
            working.setLocationRelativeTo(null);
            working.setVisible(true);
            mainSettings.setStreamLink(main_livestream_link.getText());
            mainSettings.setStreamOnline(main_livestream_checkbox.isSelected());
            working.getTextArea().append("Guardando definicoes principais: \n");
            main_handler.saveToFile(mainSettings, working.getTextArea());
            working.getTextArea().append("Guardando programa: \n");
            program_handler.setDeletedItems(deleted_program);
            program_handler.saveToFile(program_events.toArray(), working.getTextArea());
            working.getTextArea().append("Guardando novidades: \n");
            news_handler.setDeletedItems(deleted_news);
            news_handler.saveToFile(news_events.toArray(), working.getTextArea());
            working.getTextArea().append("Guardando patrocinadores: \n");
            sponsors_handler.setDeletedItems(deleted_sponsors);
            sponsors_handler.saveToFile(sponsors.toArray(), working.getTextArea());
        }
    }//GEN-LAST:event_button_saveallActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        prefs.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        JOptionPane.showMessageDialog(null, "Aplicação feita por:\nHelder Moreira", "Sobre", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void sponsors_button_upActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsors_button_upActionPerformed
        int selectedIndex = SponsorsList.getSelectedIndex();
        if (selectedIndex <= 0) {
            return;
        }
        int next_index = selectedIndex - 1;

        Sponsor sp1 = (Sponsor) sponsors.getElementAt(selectedIndex);
        Sponsor sp2 = (Sponsor) sponsors.getElementAt(next_index);

        sponsors.setElementAt(sp1, next_index);
        sponsors.setElementAt(sp2, selectedIndex);

        SponsorsList.setSelectedIndex(next_index);
    }//GEN-LAST:event_sponsors_button_upActionPerformed

    private void sponsors_button_downActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsors_button_downActionPerformed
        int selectedIndex = SponsorsList.getSelectedIndex();
        if (selectedIndex < 0 || selectedIndex >= sponsors.size() - 1) {
            return;
        }
        int next_index = selectedIndex + 1;

        Sponsor sp1 = (Sponsor) sponsors.getElementAt(selectedIndex);
        Sponsor sp2 = (Sponsor) sponsors.getElementAt(next_index);

        sponsors.setElementAt(sp1, next_index);
        sponsors.setElementAt(sp2, selectedIndex);

        SponsorsList.setSelectedIndex(next_index);
    }//GEN-LAST:event_sponsors_button_downActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SebsJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SebsJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SebsJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SebsJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            SebsJFrame sebs = new SebsJFrame();
            sebs.setLocationRelativeTo(null);
            sebs.setVisible(true);
        });
    }

    private void disableNotify() {
        news_day_combo.setEnabled(false);
        news_month_combo.setEnabled(false);
        news_hour_spin.setEnabled(false);
        news_minutes_spin.setEnabled(false);
    }

    private void enableNotify() {
        news_day_combo.setEnabled(true);
        news_month_combo.setEnabled(true);
        news_hour_spin.setEnabled(true);
        news_minutes_spin.setEnabled(true);
    }

    private void disableNews() {
        news_title_textfield.setEnabled(false);
        news_desc_textfield.setEnabled(false);
        news_image_textfield.setEnabled(false);
        news_video_textfield.setEnabled(false);
        disableNotify();
        news_notify_checkbox.setEnabled(false);
        news_ok_button.setEnabled(false);
        news_cancel_button.setEnabled(false);
    }

    private void enableNews() {
        news_title_textfield.setEnabled(true);
        news_desc_textfield.setEnabled(true);
        news_image_textfield.setEnabled(true);
        news_video_textfield.setEnabled(true);
        enableNotify();
        news_notify_checkbox.setEnabled(true);
        news_ok_button.setEnabled(true);
        news_cancel_button.setEnabled(true);
    }

    private void clearNews() {
        news_title_textfield.setText("");
        news_desc_textfield.setText("");
        news_image_textfield.setText("");
        news_video_textfield.setText("");
        news_day_combo.setSelectedIndex(0);
        news_month_combo.setSelectedIndex(6);
        news_hour_spin.setValue(22);
        news_minutes_spin.setValue(30);
        news_notify_checkbox.setSelected(false);
    }

    private void disableProgram() {
        program_title_textfield.setEnabled(false);
        program_desc_textfield.setEnabled(false);
        program_image_textfield.setEnabled(false);
        program_video_textfield.setEnabled(false);
        program_day_combo.setEnabled(false);
        program_month_combo.setEnabled(false);
        program_hour_spin.setEnabled(false);
        program_minutes_spin.setEnabled(false);
        program_ok_button.setEnabled(false);
        program_cancel_button.setEnabled(false);
    }

    private void enableProgram() {
        program_title_textfield.setEnabled(true);
        program_desc_textfield.setEnabled(true);
        program_image_textfield.setEnabled(true);
        program_video_textfield.setEnabled(true);
        program_day_combo.setEnabled(true);
        program_month_combo.setEnabled(true);
        program_hour_spin.setEnabled(true);
        program_minutes_spin.setEnabled(true);
        program_ok_button.setEnabled(true);
        program_cancel_button.setEnabled(true);
    }

    private void clearProgram() {
        program_title_textfield.setText("");
        program_desc_textfield.setText("");
        program_image_textfield.setText("");
        program_video_textfield.setText("");
        program_day_combo.setSelectedIndex(0);
        program_month_combo.setSelectedIndex(6);
        program_hour_spin.setValue(22);
        program_minutes_spin.setValue(30);
    }

    private void disableSponsors() {
        sponsors_image_textfield.setEnabled(false);
        sponsors_title_textfield.setEnabled(false);
        sponsors_link_textfield.setEnabled(false);
        sponsors_ok_button.setEnabled(false);
        sponsors_cancel_button.setEnabled(false);
    }

    private void enableSponsors() {
        sponsors_image_textfield.setEnabled(true);
        sponsors_title_textfield.setEnabled(true);
        sponsors_link_textfield.setEnabled(true);
        sponsors_ok_button.setEnabled(true);
        sponsors_cancel_button.setEnabled(true);
    }

    private void clearSponsors() {
        sponsors_title_textfield.setText("");
        sponsors_image_textfield.setText("");
        sponsors_link_textfield.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList NewsList;
    private javax.swing.JList ProgramList;
    private javax.swing.JList SponsorsList;
    private javax.swing.JButton button_saveall;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JCheckBox main_livestream_checkbox;
    private javax.swing.JTextField main_livestream_link;
    private javax.swing.JPanel main_livestream_panel;
    private javax.swing.JButton news_button_delete;
    private javax.swing.JButton news_button_edit;
    private javax.swing.JButton news_button_new;
    private javax.swing.JButton news_cancel_button;
    private javax.swing.JComboBox news_day_combo;
    private javax.swing.JTextArea news_desc_textfield;
    private javax.swing.JSpinner news_hour_spin;
    private javax.swing.JTextField news_image_textfield;
    private javax.swing.JSpinner news_minutes_spin;
    private javax.swing.JComboBox news_month_combo;
    private javax.swing.JCheckBox news_notify_checkbox;
    private javax.swing.JButton news_ok_button;
    private javax.swing.JTextField news_title_textfield;
    private javax.swing.JTextField news_video_textfield;
    private javax.swing.JPanel panel_main;
    private javax.swing.JPanel panel_news;
    private javax.swing.JPanel panel_program;
    private javax.swing.JPanel panel_sponsors;
    private javax.swing.JButton program_button_delete;
    private javax.swing.JButton program_button_edit;
    private javax.swing.JButton program_button_new;
    private javax.swing.JButton program_cancel_button;
    private javax.swing.JComboBox program_day_combo;
    private javax.swing.JTextArea program_desc_textfield;
    private javax.swing.JSpinner program_hour_spin;
    private javax.swing.JTextField program_image_textfield;
    private javax.swing.JSpinner program_minutes_spin;
    private javax.swing.JComboBox program_month_combo;
    private javax.swing.JButton program_ok_button;
    private javax.swing.JTextField program_title_textfield;
    private javax.swing.JTextField program_video_textfield;
    private javax.swing.JButton sponsors_button_delete;
    private javax.swing.JButton sponsors_button_down;
    private javax.swing.JButton sponsors_button_edit;
    private javax.swing.JButton sponsors_button_new;
    private javax.swing.JButton sponsors_button_up;
    private javax.swing.JButton sponsors_cancel_button;
    private javax.swing.JTextField sponsors_image_textfield;
    private javax.swing.JTextField sponsors_link_textfield;
    private javax.swing.JButton sponsors_ok_button;
    private javax.swing.JTextField sponsors_title_textfield;
    // End of variables declaration//GEN-END:variables
}
