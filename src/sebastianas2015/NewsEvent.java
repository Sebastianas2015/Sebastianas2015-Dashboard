package sebastianas2015;

import java.util.UUID;

/**
 *
 * @author helder
 */
public class NewsEvent implements Comparable<NewsEvent> {
    
    long uuid;
    String title="", desc="", image="", video="";
    boolean notify;
    int day, month, hour, minutes;

    public void setUUID(long uuid){
        this.uuid = uuid;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }
    
    public NewsEvent(){
        this.uuid = UUID.randomUUID().getMostSignificantBits();
    }

    public NewsEvent(String title, String desc,
            String image, String video, int day,
            int month, int hour, int minutes, boolean notify) {

        this.uuid = UUID.randomUUID().getMostSignificantBits();
        this.title = title;
        this.desc = desc;
        this.image = image;
        this.video = video;
        this.day = day;
        this.month = month;
        this.hour = hour;
        this.minutes = minutes;
        this.notify = notify;
    }

    @Override
    public String toString() {
        return String.format("%02d", day) + "/" + String.format("%02d", month) + " às "
                + String.format("%02d", hour) + ":" + String.format("%02d", minutes) + " - " + title;
    }
    
    @Override
    public boolean equals(Object obj){
        NewsEvent event = (NewsEvent) obj;
        return this.uuid == event.uuid;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }
    

    @Override
    public int compareTo(NewsEvent o) {
        if (this.month != o.month)
            return this.month - o.month;
        if (this.day != o.day)
            return this.day - o.day;
        if (this.hour != o.hour)
            return this.hour - o.hour;
        if (this.minutes != o.minutes)
            return this.minutes - o.minutes;
        return 0;
    }

}
