package sebastianas2015;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import org.apache.commons.net.ftp.FTPClient;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author helder
 */
public class SponsorsHandler {

    private static final String FILENAME = "sponsors";

    private final String paddress;
    private final String ftp_address;
    private final String ftp_folder;
    private final String username;
    private final String password;

    private ArrayList<Long> deleted;

    public SponsorsHandler(String paddress, String ftp_address, String ftp_folder, String username, String password) {
        this.paddress = paddress;
        this.ftp_address = ftp_address;
        this.username = username;
        this.password = password;
        this.ftp_folder = ftp_folder;
        deleted = new ArrayList();
    }

    public Document parse(URL url) {
        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read(url);
            return document;
        } catch (DocumentException ex) {
            return null;
        }
    }

    public void setDeletedItems(ArrayList<Long> list) {
        deleted = list;
    }

    public ArrayList<Long> getDeletedItems() {
        return deleted;
    }

    public Sponsor[] loadFromFile() {
        URL url = null;
        try {
            url = new URL(paddress + FILENAME + ".xml");
        } catch (MalformedURLException ex) {
            Logger.getLogger(SponsorsHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        Document doc = parse(url);

        if (doc == null) {
            return null;
        }

        Queue<Sponsor> sponsors = new PriorityQueue<>();
        Element root = doc.getRootElement();

        for (Iterator i = root.elementIterator(); i.hasNext();) {
            Element item = (Element) i.next();
            if (item.getName().equals("event")) {
                Sponsor sponsor = new Sponsor();
                for (Iterator j = item.elementIterator(); j.hasNext();) {
                    Element el = (Element) j.next();
                    switch (el.getName()) {
                        case "uuid":
                            sponsor.setUUID(Long.parseLong((String) el.getData()));
                            break;
                        case "title":
                            sponsor.setTitle((String) el.getData());
                            break;
                        case "image":
                            sponsor.setImage((String) el.getData());
                            break;
                        case "link":
                            sponsor.setLink((String) el.getData());
                            break;
                    }
                }
                sponsors.add(sponsor);
            } else {
                for (Iterator j = item.elementIterator(); j.hasNext();) {
                    Element el = (Element) j.next();
                    deleted.add(Long.parseLong((String) el.getData()));
                }
            }

        }

        Sponsor[] array = new Sponsor[sponsors.size()];
        return sponsors.toArray(array);
    }

    public boolean saveToFile(Object[] objs, JTextArea textarea) {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(FILENAME);

        Sponsor[] program = Arrays.asList(objs).toArray(new Sponsor[objs.length]);

        textarea.append("Criando documento... ");
        textarea.update(textarea.getGraphics());
        for (Sponsor event : program) {
            Element xml_event = root.addElement("event");
            xml_event.addElement("uuid")
                    .addText("" + event.uuid);
            xml_event.addElement("title")
                    .addText(event.title);
            if (!event.image.equals("")) {
                xml_event.addElement("image")
                        .addText(event.image);
            }
            if (!event.link.equals("")) {
                xml_event.addElement("link")
                        .addText(event.link);
            }
        }
        Element xml_deletedEvents = root.addElement("deleted_sponsors");
        deleted.stream().forEach((uuid) -> {
            xml_deletedEvents.addElement("deleted_sponsor")
                    .addText("" + uuid);
        });
        textarea.append("OK\n");
        textarea.update(textarea.getGraphics());
        return saveDocument(document, textarea);
    }

    public boolean saveDocument(Document document, JTextArea textarea) {

        try {
            textarea.append("Guardando em ficheiro temporario... ");
            textarea.update(textarea.getGraphics());
            File file = File.createTempFile(FILENAME, ".xml");
            OutputFormat format = OutputFormat.createPrettyPrint();
            XMLWriter writer = new XMLWriter(new FileWriter(file), format);
            writer.write(document);
            writer.close();
            textarea.append("OK\n");
            textarea.update(textarea.getGraphics());
            return sendFile(file, textarea);
        } catch (IOException ex) {
            textarea.append("ERRO!!\n");
            textarea.update(textarea.getGraphics());
            Logger.getLogger(SponsorsHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean sendFile(File file, JTextArea textarea) {
        FTPClient client = new FTPClient();
        FileInputStream fis = null;
        boolean result = false;
        try {
            textarea.append("Criando ligação por FTP ao servidor... ");
            textarea.update(textarea.getGraphics());
            client.connect(ftp_address);
            textarea.append("OK\nEfectuando login... ");
            textarea.update(textarea.getGraphics());
            client.login(username, password);
            textarea.append("OK\nGuardando ficheiro no servidor... ");
            textarea.update(textarea.getGraphics());
            fis = new FileInputStream(file);
            client.storeFile(ftp_folder + FILENAME + ".xml", fis);
            textarea.append("OK\nEfectuando logout... ");
            textarea.update(textarea.getGraphics());
            client.logout();
            textarea.append("OK\nApagando Ficheiro temporario... ");
            textarea.update(textarea.getGraphics());
            file.delete();
            textarea.append("OK\n");
            textarea.update(textarea.getGraphics());
            result = true;
        } catch (IOException ex) {
            textarea.append("ERRO!!\n");
            textarea.update(textarea.getGraphics());
            Logger.getLogger(SponsorsHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                textarea.append("Desligando do servidor... ");
                textarea.update(textarea.getGraphics());
                client.disconnect();
                textarea.append("OK\n");
            } catch (IOException ex) {
                textarea.append("ERRO!!\n");
                Logger.getLogger(SponsorsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

}
